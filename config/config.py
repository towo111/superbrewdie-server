
# Consider temperature reached if within this delta. [deg celsius]
HEATING_TEMPERATURE_REACHED_DELTA = 3.0

# Temperature when the water can be considered cooking. [deg celsius]
COOKING_TEMPERATURE = 98
