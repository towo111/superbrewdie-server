from interfaces.actor import Actor
from interfaces.sensor import Sensor


class SimulatedTemperatureSensor(Sensor):
    def __init__(self, actor: Actor) -> None:
        super().__init__()

        self._time = -1
        self._temperature = 20

        self._actor = actor

    def sense(self, time: int) -> bool:
        """
        Uses sine of the time to come up with an artificial temperature measurement.

        Args:
            time: Current time of calling this method.

        Returns:
            Whether sensing was successful.
        """
        super().sense(time)

        if self._time in self._actor.history.history:
            if self._actor.history.history[self._time]:
                self._temperature += 3.0
            else:
                self._temperature -= 1.0

        self._time = time
        print('{}\t[{}]: {:3.2f} deg'.format(self._time, self.__class__, self._temperature))

        # Store the measurement to the history.
        self.store(self._time, self._temperature)

        return True

    def get_time(self):
        return self._time

    def get_value(self):
        return self._temperature

