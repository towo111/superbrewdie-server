from w1thermsensor import W1ThermSensor

from interfaces.sensor import Sensor


class OneWireTemperatureSensor(Sensor):
    def __init__(self) -> None:
        super().__init__()

        self._sensor = W1ThermSensor()

        self._time = 0
        self._temperature = 0.0

    def sense(self, time: int) -> bool:
        self._time = time
        self._temperature = self._sensor.get_temperature()

        self.store(self._time, self._temperature)

    def get_time(self):
        return self._time

    def get_value(self):
        return self._temperature



