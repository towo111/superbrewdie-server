from interfaces.actor import Actor


class SimulatedActor(Actor):
    def __init__(self) -> None:
        super().__init__()

        self._enabled = False

    def start(self, time: int) -> bool:
        print('{}\t[{}]: {}'.format(time, self.__class__, 'ON!'))
        self._enabled = True

        self.store(time, self._enabled)

        return True

    def stop(self, time) -> bool:
        print('{}\t[{}]: {}'.format(time, self.__class__, 'OFF!'))

        self._enabled = False

        self.store(time, self._enabled)

        return True
