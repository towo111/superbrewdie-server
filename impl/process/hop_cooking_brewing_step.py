from config.config import COOKING_TEMPERATURE
from impl.controller.heating_controller import HeatingController
from impl.data.brewing_step import HopCookingBrewingStepData
from impl.logger import logger
from impl.process.brewing_step import BrewingStep
from impl.process.state import State


class HopCookingBrewingStep(BrewingStep):
    def __init__(self, data: HopCookingBrewingStepData, heater_controller: HeatingController) -> None:
        super().__init__()

        self._state = State('HOP_COOKING', 'INVALID', 'INVALID')

        self._data = data

        self._heater_controller = heater_controller

    def init(self):
        super().init()

        self._state.request('START')

        self._heater_controller.init()
        self._heater_controller.actor.set_history(self._data.get_heating_history())
        self._heater_controller.sensor.set_history(self._data.get_temperature_history())

    def exec_cycle(self, time) -> State:
        # Each cycle, give the controller the chance to do something.
        self._heater_controller.exec(time)

        # Check whether hop additions are needed.
        if self._heater_controller.has_reached_target_temperature():
            duration_since_cooking = time - self._heater_controller.get_reached_temperature_time()

            hops_to_add = list()
            for hop_addition in self._data.hop_additions:
                time_until_addition = hop_addition.hop_addition.input_time - duration_since_cooking
                if time_until_addition > 0:
                    logger.info('[HopCooking] {} seconds until addition of {}'.format(time_until_addition,
                                                                                      hop_addition.hop_addition.name))
                elif time_until_addition <= 0 and hop_addition.added_time < 0:
                    logger.info('[HopCooking] Need to add {} now!'.format(hop_addition.hop_addition.name))
                    hops_to_add.append(hop_addition.hop_addition)

            if len(hops_to_add) > 0:
                self._state.request('ADDITION', hops_to_add)

            # If it's cooking for expected time, let's stop!
            if duration_since_cooking >= self._data.duration:
                self._heater_controller.stop(time)
                self._data.end_time = time
                self._state.end()

        return self._state

    def confirm_request(self, time):
        # Check if there is actually a request, if not, do nothing.
        if self._state.is_awaiting_answer():
            state_position = self._state.get_position()

            if state_position == 'START':
                self._handle_start_confirmation(time)
            elif state_position == 'ADDITION':
                self._handle_addition_confirmation(time)

    def get_data(self):
        return self._data

    def _handle_start_confirmation(self, time):
        # Update the state
        self._state.confirm_request('ONGOING')

        logger.info('[HopCooking] Start heating for hop cooking.')

        # Setup controller to start heating.
        self._heater_controller.set_target_temperature(COOKING_TEMPERATURE)
        self._heater_controller.start(time)

    def _handle_addition_confirmation(self, time: int):
        if self._heater_controller.has_reached_target_temperature():
            duration_since_cooking = time - self._heater_controller.get_reached_temperature_time()

            added_something = False
            for hop_addition in self._data.hop_additions:
                if hop_addition.hop_addition.input_time < duration_since_cooking and hop_addition.added_time < 0:
                    hop_addition.added_time = time
                    added_something = True

            if added_something:
                self._state.confirm_request('ONGOING')
