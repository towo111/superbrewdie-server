from impl.controller.heating_controller import HeatingController
from impl.data.brewing_step import MashingBrewingStepData
from impl.data.mashing_rast import MashingRast
from impl.logger import logger
from impl.process.brewing_step import BrewingStep
from impl.process.state import State


class MashingBrewingStep(BrewingStep):
    def __init__(self, data: MashingBrewingStepData, heater_controller: HeatingController) -> None:
        super().__init__()

        self._state = State('MASHING', 'INVALID', 'INVALID')
        self._current_rast_idx = -1

        self._data = data

        self._heater_controller = heater_controller

    def init(self):
        self._state.request('START')

        self._heater_controller.init()
        self._heater_controller.actor.set_history(self._data.get_heating_history())
        self._heater_controller.sensor.set_history(self._data.get_temperature_history())

        super().init()

    def confirm_request(self, time):
        # Check if there is actually a request, if not, do nothing.
        if self._state.is_awaiting_answer():
            state_position = self._state.get_position()

            # Change to the next step according to the process.
            if state_position == 'START':
                self._handle_start_confirmation(time)
            elif state_position == 'IODINE':
                self._handle_iodine_test(time)
            elif state_position == 'ADDITION':
                self._handle_mash_addition(time)

    def get_data(self):
        return self._data

    def get_current_rast(self) -> MashingRast:
        return self._data.rasts[self._current_rast_idx]

    def _start_heating_rast(self, rast: MashingRast, time):
        logger.info('[Mashing] Start heating for rast: {}'.format(rast))

        # Setup controller to start heating.
        self._heater_controller.set_target_temperature(rast.get_target_temperature())
        self._heater_controller.start(time)

        rast.started_heating_time = time

    def _finish_mashing(self, time: int):
        self.get_current_rast().finished_rast_time = time

        self._heater_controller.stop(time)

        self._data.end_time = time

        self._state.end()

    def _start_next_rast(self, time):
        current_rast = self.get_current_rast()
        current_rast.finished_rast_time = time

        self._current_rast_idx += 1
        next_rast = self.get_current_rast()
        self._start_heating_rast(next_rast, time)

    def _handle_start_confirmation(self, time):
        # Update the state
        self._state.confirm_request('ONGOING')

        if len(self._data.rasts) > 0:
            self._current_rast_idx = 0

            start_rast = self.get_current_rast()

            self._start_heating_rast(start_rast, time)

            self._data.start_time = time
        else:
            logger.error('No rasts available!')

    def _handle_iodine_test(self, time):
        # Update the state
        self._state.confirm_request('ONGOING')

        # todo: handle iodine test

    def _handle_mash_addition(self, time):
        # Update the state
        self._state.confirm_request('ONGOING')

        for malt_addition in self._data.malt_additions:
            malt_addition.added_time = time

        self._data.malt_added_time = time
        self.get_current_rast().temperature_reached_time = time

    def exec_cycle(self, time) -> State:
        # Each cycle, give the controller the chance to do something.
        self._heater_controller.exec(time)

        # If there is a request, just return, nothing to do right now.
        if self._state.is_awaiting_answer():
            return self._state

        # Let's do what needs to be done in the current step.
        if self._state.get_position() == 'ONGOING':
            if self._heater_controller.has_reached_target_temperature():
                # Do we already consider mashing to be started?
                # It really just starts when the malt has been added.
                if self._data.malt_added_time < 0:
                    self._state.request('ADDITION', [m.malt_addition for m in self._data.malt_additions])
                else:
                    # Malt is already in. So just heating from now on and checking when to start next rast.
                    current_rast = self.get_current_rast()

                    if current_rast.temperature_reached_time < 0:
                        current_rast.temperature_reached_time = self._heater_controller.get_reached_temperature_time()

                    duration_current_rast = time - current_rast.temperature_reached_time

                    if duration_current_rast >= current_rast.get_duration():
                        if self._current_rast_idx < len(self._data.rasts) - 1:
                            self._start_next_rast(time)
                        else:
                            self._finish_mashing(time)
                    else:
                        logger.info('[Mashing] {} seconds left for current rast.'
                                    .format(current_rast.get_duration() - duration_current_rast))

        return self._state

