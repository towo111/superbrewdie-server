from impl.data.brewing_step import BrewingStepData
from impl.process.state import State


class BrewingStepStateMashine(object):

    def __init__(self) -> None:
        pass


class BrewingStep(object):
    def __init__(self) -> None:
        self._initialized = False

        self._state = State()

    def is_initialized(self) -> bool:
        return self._initialized

    def init(self):
        self._initialized = True

    def exec_cycle(self, time) -> State:
        pass

    def confirm_request(self, time):
        pass

    def get_state(self):
        return self._state

    def get_data(self):
        pass


class ConfirmationStep(BrewingStep):
    def __init__(self, state_step: str, data: BrewingStepData) -> None:
        super().__init__()

        self._state = State(state_step, 'INVALID', 'INVALID')

        self._data = data

    def init(self):
        self._state.request('START')

        super().init()

    def exec_cycle(self, time) -> State:
        return self._state

    def confirm_request(self, time):
        if self._state.is_awaiting_answer():
            if self._state.get_position() == 'START':
                self._data.start_time = time

                self._state.confirm_request('ONGOING')
                self._state.request('END')
            else:
                self._data.end_time = time

                self._state.confirm_request('END')

    def get_data(self):
        return self._data


