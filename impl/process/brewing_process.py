from typing import List

from impl.process.brewing_step import BrewingStep
from impl.process.state import State


class BrewingProcess(object):
    _steps: List[BrewingStep]

    def __init__(self) -> None:
        self._steps = []
        self._active_step_idx = -1

    def add_step(self, step: BrewingStep) -> None:
        self._steps.append(step)

    def start(self):
        assert len(self._steps) > 0, "Need to setup some steps in order to start a process!"

        self._active_step_idx = 0

    def _start_next_step(self):
        self._active_step_idx += 1

    def get_active_step(self):
        return self._steps[self._active_step_idx]

    def exec_cycle(self, time: int) -> State:
        current_step = self._steps[self._active_step_idx]

        if not current_step.is_initialized():
            current_step.init()

        state = current_step.exec_cycle(time)

        if state.get_position() == 'END' and not state.is_awaiting_answer():
            self._start_next_step()

            if not self._active_step_idx < len(self._steps):
                # Process is finished!
                state = State('FINISHED', 'END', 'NORMAL')

        return state

    def confirm_request(self, time: int):
        self._steps[self._active_step_idx].confirm_request(time)

    def get_state(self):
        if self._active_step_idx < 0:
            return State()
        elif self._active_step_idx < len(self._steps):
            return self._steps[self._active_step_idx].get_state()
        else:
            return State('FINISHED', 'END', 'NORMAL')
