import json
from typing import Dict

from impl.logger import logger


class State(object):
    STEPS: Dict[str, int] = {
        'NOT_STARTED': 100,
        'MASHING': 200,
        'LAUTERING': 300,
        'HOP_COOKING': 400,
        'WHIRLPOOL': 500,
        'FINISHED': 600
    }
    POSITIONS: Dict[str, int] = {
        'INVALID': 0,
        'START': 10,
        'ONGOING': 20,
        'END': 30,
        'IODINE': 40,
        'ADDITION': 50
    }
    TYPES: Dict[str, int] = {
        'INVALID': 0,
        'NORMAL': 1,
        'REQUEST': 2,
        'CANCELLED': 3
    }

    _step: str = 'NOT_STARTED'
    _position: str = 'INVALID'
    _type: str = 'INVALID'

    def __init__(self,
                 init_step: str = 'NOT_STARTED',
                 init_position: str = 'INVALID',
                 init_type: str = 'INVALID') -> None:
        self.set_step(init_step)
        self.set_position(init_position)
        self.set_type(init_type)

        self._additional_data = None

    def __eq__(self, other):
        return other.get_step() == self._step \
               and other.get_position() == self._position \
               and other.get_type() == self._type

    def get_step(self):
        return self._step

    def set_step(self, new_step):
        if new_step not in State.STEPS:
            raise AttributeError('{} not in State.STEPS'.format(new_step))

        self._step = new_step

    def get_position(self):
        return self._position

    def set_position(self, new_position):
        if new_position not in State.POSITIONS:
            raise AttributeError('{} not in State.STEPS'.format(new_position))

        self._position = new_position

    def get_type(self):
        return self._type

    def set_type(self, new_type):
        if new_type not in State.TYPES:
            raise AttributeError('{} not in State.STEPS'.format(new_type))

        self._type = new_type

    def is_awaiting_answer(self):
        return self._type == 'REQUEST'

    def to_int(self):
        return State.STEPS[self._step] + State.POSITIONS[self._position] + State.TYPES[self._type]

    def request(self, position: str, data: list = None):
        logger.info('[STATE] Change state! Request {} (data: {})'.format(position, data))

        self._additional_data = data

        self.set_position(position)
        self.set_type('REQUEST')

    def confirm_request(self, new_position):
        logger.info('[STATE] Confirmed state ({}). Transition to position \'{}\''.format(self.to_int(), new_position))

        self._additional_data = None

        self.set_position(new_position)
        self.set_type('NORMAL')

    def __str__(self) -> str:
        return '{} ({}, {}, {})'.format(self.to_int(), self._step, self._position, self._type)

    def assign_state(self, state):
        self._step = state.get_step()
        self._position = state.get_position()
        self._type = state.get_type()

    def end(self):
        self.set_position('END')
        self.set_type('NORMAL')

    def to_dict(self):
        return {
            'state': self.to_int(),
            'detail': {
                'step': self._step,
                'position': self._position,
                'type': self._type
            }
        }

    @staticmethod
    def documentation():
        return 'Three digit number:\n'\
               ' - First digit: Step ({})\n'\
               ' - Second digit: Position ({})\n'\
               ' - Third digit: Type ({})'.format(State.STEPS, State.POSITIONS, State.TYPES)
