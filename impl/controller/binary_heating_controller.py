from config.config import HEATING_TEMPERATURE_REACHED_DELTA
from impl.controller.heating_controller import HeatingController
from interfaces.actor import Actor
from interfaces.sensor import Sensor


class BinaryHeatingController(HeatingController):
    def __init__(self, actor: Actor, sensor: Sensor):
        super().__init__(actor, sensor)

    def exec(self, time):
        if super()._is_controlling_possible():
            # Get current temperature
            if self.sensor.sense(time):
                current_temperature = self.sensor.get_value()

                # If close to target temperature, consider it being reached,
                # so only keeping the temperature needs to be done.
                if not self.has_reached_target_temperature() \
                        and abs(current_temperature - self._target_temperature) < HEATING_TEMPERATURE_REACHED_DELTA:
                    self.set_temperature_reached(time)

                # If below temperature, just enable the heating. If above, disable it.
                if current_temperature < self._target_temperature:
                    self.actor.start(time)
                else:
                    self.actor.stop(time)
