from impl.logger import logger
from interfaces.actor import Actor
from interfaces.actor_controller import ActorController
from interfaces.sensor import Sensor


class HeatingController(ActorController):
    def __init__(self, actor: Actor, sensor: Sensor):
        super(HeatingController, self).__init__(actor, sensor)

        self._reached_temperature_time = -1

        self._controlling = False
        self._target_temperature = None

    def init(self):
        self._reached_temperature_time = -1
        self._target_temperature = None

    def start(self, time):
        self._controlling = True

    def stop(self, time):
        self.actor.stop(time)
        self._controlling = False

    def _is_controlling_possible(self):
        return self._controlling and self._target_temperature is not None

    def set_target_temperature(self, temperature: float) -> None:
        self._reached_temperature_time = -1
        self._target_temperature = temperature

    def set_temperature_reached(self, time: int) -> None:
        logger.info('[Heating] Reached target temperature of {} (actual temperature: {})'
                    .format(self._target_temperature, self.sensor.get_value()))
        self._reached_temperature_time = time

    def has_reached_target_temperature(self) -> bool:
        return self._reached_temperature_time > 0

    def get_reached_temperature_time(self) -> int:
        return self._reached_temperature_time
