class BrewingServerStateMashine(object):
    def __init__(self) -> None:
        self._states = ['idle', 'mashing', 'lautering', 'hop_cooking', 'whirlpool', 'finished']
        self._transitions = {
            'idle': 'mashing',
            'mashing': 'lautering'
        }

