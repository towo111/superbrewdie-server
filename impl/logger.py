import time
from datetime import datetime
from typing import List
import logging


class LogMsg(object):
    _time: int
    _level: str
    _message: str

    def __init__(self, level: str, message: str) -> None:
        self._time = int(time.time())
        self._level = level
        self._message = message

    def __str__(self) -> str:
        return '[{}] {} - {}'.format(datetime.utcfromtimestamp(self._time).strftime('%Y-%m-%d %H:%M:%S'),
                                     self._level.upper(), self._message)

    def get_level(self):
        return self._level

    def get_message(self):
        return self._message


class Logger(object):
    messages: List[LogMsg]

    def __init__(self) -> None:
        super().__init__()

        self.messages = list()

    def info(self, message, *args, **kwargs) -> None:
        self.messages.append(LogMsg('info', message))

        logging.info(message, *args, **kwargs)

    def warning(self, message, *args, **kwargs) -> None:
        self.messages.append(LogMsg('warn', message))

        logging.warning(message, *args, **kwargs)

    def error(self, message, *args, **kwargs) -> None:
        self.messages.append(LogMsg('error', message))

        logging.error(message, *args, **kwargs)


logger = Logger()
