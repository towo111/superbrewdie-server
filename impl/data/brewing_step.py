from typing import List

from config.constants import THERMOMETER_NAME, HEATER_NAME
from impl.data.actor_history import ActorHistory
from impl.data.ingredient import MaltAdditionData, HopAdditionData
from impl.data.mashing_rast import MashingRast
from impl.data.recipe import MaltAddition, HopAddition
from impl.data.sensor_history import SensorHistory


class BrewingStepData(object):
    def __init__(self, name) -> None:
        self._name = name

        self.start_time = -1
        self.end_time = -1

        self._actors = list()
        self._actors_history = dict()

        self._sensors = list()
        self._sensors_history = dict()

    def get_name(self):
        return self._name

    def add_actor(self, name):
        self._actors.append(name)
        self._actors_history[name] = ActorHistory()

    def get_actor_history(self, name) -> ActorHistory:
        return self._actors_history[name]

    def add_sensor(self, name):
        self._sensors.append(name)
        self._sensors_history[name] = SensorHistory()

    def get_sensor_history(self, name) -> SensorHistory:
        return self._sensors_history[name]


class HeatingBrewingStepData(BrewingStepData):

    def __init__(self, name) -> None:
        super().__init__(name)

        self.add_sensor(THERMOMETER_NAME)
        self.add_actor(HEATER_NAME)

    def get_temperature_history(self) -> SensorHistory:
        return self.get_sensor_history(THERMOMETER_NAME)

    def get_heating_history(self) -> ActorHistory:
        return self.get_actor_history(HEATER_NAME)


class MashingBrewingStepData(HeatingBrewingStepData):
    rasts: List[MashingRast]
    malt_additions: List[MaltAdditionData]

    def __init__(self, name) -> None:
        super().__init__(name)

        self.malt_added_time = -1

        self.rasts = list()
        self.malt_additions = list()

    def add_rast(self, temperature, duration):
        self.rasts.append(MashingRast(temperature, duration))

    def add_malt_addition(self, malt_addition: MaltAddition):
        self.malt_additions.append(MaltAdditionData(malt_addition))


class HopCookingBrewingStepData(HeatingBrewingStepData):
    hop_additions: List[HopAdditionData]

    def __init__(self, name, duration: int) -> None:
        super().__init__(name)

        self.hop_additions = list()

        self.duration = duration

    def add_hop_addition(self, hop_addition: HopAddition):
        self.hop_additions.append(HopAdditionData(hop_addition))
