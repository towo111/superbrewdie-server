from typing import List


class Ingredient(object):
    # The name of the ingredient.
    name: str
    # Amount of the ingredient to be added given the unit.
    amount: float
    # Unit for the amount.
    unit: str

    def __init__(self, name: str, amount: float, unit: str) -> None:
        self.name = name
        self.amount = amount
        self.unit = unit


class IngredientAddition(Ingredient):
    # Time since the start of the respective step when the ingredient should be added. [seconds]
    input_time: int

    def __init__(self, name: str, amount: float, unit: str, input_time: int) -> None:
        super().__init__(name, amount, unit)

        self.input_time = input_time


class MaltAddition(IngredientAddition):
    pass


class HopAddition(IngredientAddition):
    pass


class TemperatureLevel(object):
    # The temperature of the rast in deg celsius.
    temperature: float
    # Duration of the rast in seconds.
    duration: int

    def __init__(self, temperature: float, duration: int) -> None:
        self.temperature = temperature
        self.duration = duration


class MashingPlan(object):
    # List of malt to be added for mashing.
    malt_additions: List[MaltAddition]
    # Temperature levels which need to be done for the beer.
    temperature_levels: List[TemperatureLevel]

    def __init__(self) -> None:
        self.malt_additions = list()
        self.temperature_levels = list()


class HopCookingPlan(object):
    # List of hop to be added for mashing.
    hop_additions: List[HopAddition]
    # Total duration of the hop cooking in seconds.
    duration: int

    def __init__(self) -> None:
        self.hop_additions = list()
        self.duration = 0


class Recipe(object):
    # Unique id for the recipe.
    identifier: str
    # Name of the recipe.
    name: str
    # Description of the recipe.
    description: str
    # Date this recipe was created.
    date: int
    # The plan for mashing step.
    mashing_plan: MashingPlan
    # The plan for the hop cooking step.
    hop_cooking_plan: HopCookingPlan

    def __init__(self, identifier: str, name: str, description: str, date: int) -> None:
        self.identifier = identifier
        self.name = name
        self.description = description
        self.date = date

        self.mashing_plan = MashingPlan()
        self.hop_cooking_plan = HopCookingPlan()
