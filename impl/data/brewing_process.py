from impl.data.brewing_step import BrewingStepData


class BrewingProcessData(object):
    def __init__(self) -> None:
        self._brewing_steps = dict()

    def add_brewing_step(self, brewing_step: BrewingStepData):
        self._brewing_steps[brewing_step.get_name()] = brewing_step

    def get_brewing_step(self, name) -> BrewingStepData:
        return self._brewing_steps[name]
