class MashingRast(object):
    def __init__(self, temperature, duration) -> None:
        self._target_temperature = temperature
        self._duration = duration

        self.started_heating_time = -1
        self.temperature_reached_time = -1
        self.finished_rast_time = -1

    def get_target_temperature(self):
        return self._target_temperature

    def get_duration(self):
        return self._duration

    def __str__(self) -> str:
        return 'Target temperature: {}; Duration: {}; started: {}, reached temp: {}, finished: {}'\
            .format(self._target_temperature, self._duration, self.started_heating_time, self.temperature_reached_time,
                    self.finished_rast_time)


