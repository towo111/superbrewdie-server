from impl.data.recipe import MaltAddition, HopAddition


class MaltAdditionData(object):
    def __init__(self, malt_addition: MaltAddition) -> None:
        self.malt_addition = malt_addition
        self.added_time = -1

    def set_added(self, time: int):
        self.added_time = time


class HopAdditionData(object):
    def __init__(self, hop_addition: HopAddition) -> None:
        self.hop_addition = hop_addition
        self.added_time = -1

    def set_added(self, time: int):
        self.added_time = time
