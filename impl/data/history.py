class History(object):
    def __init__(self) -> None:
        self.history = dict()

    def add_item(self, time: int, item):
        self.history[time] = item
