import time

from impl.actor.simulated_actor import SimulatedActor
from impl.controller.binary_heating_controller import BinaryHeatingController
from impl.data.brewing_process import BrewingProcessData
from impl.data.brewing_step import MashingBrewingStepData, BrewingStepData, HopCookingBrewingStepData
from impl.data.recipe import Recipe
from impl.logger import logger
from impl.process.brewing_process import BrewingProcess
from impl.process.brewing_step import ConfirmationStep
from impl.process.hop_cooking_brewing_step import HopCookingBrewingStep
from impl.process.mashing_brewing_step import MashingBrewingStep
from impl.process.state import State
from impl.sensor.simulated_temperature_sensor import SimulatedTemperatureSensor


class BrewingServer(object):
    _process: BrewingProcess

    def __init__(self):
        self._running = False

        # Process
        self._process = BrewingProcess()
        self._recipe = None
        self._brewing_process_data = BrewingProcessData()

        # Hardware
        self.sensor = None
        self.actor = None
        self.controller = None

        self.setup_hardware()

    def get_current_brewing_step_data(self):
        if not self.is_running():
            return None
        else:
            return self._process.get_active_step().get_data()

    def get_brewing_process_data(self) -> BrewingProcessData:
        return self._brewing_process_data

    def setup_hardware(self):
        self.actor = SimulatedActor()
        self.sensor = SimulatedTemperatureSensor(self.actor)

        self.controller = BinaryHeatingController(self.actor, self.sensor)

    def run(self, sleep_time=5):
        if self._recipe is None or self._process is None:
            return

        self._running = True

        self._process.start()

        last_state = State()
        while self._running:
            state = self._process.exec_cycle(int(time.time()))

            if state.is_awaiting_answer() and last_state != state:
                logger.info('Process is expecting a confirmation! ' + str(state))

            if state.get_step() == 'FINISHED':
                logger.info('Finished brewing! Congratulations!')
                self._running = False

                # TODO: Make sure everything is properly saved.

            last_state.assign_state(state)
            time.sleep(sleep_time)

    def is_running(self):
        return self._running

    def init_recipe(self, recipe: Recipe):
        self._recipe = recipe
        # todo: Use actual data from the recipe.

        # # Setup the data
        process_data = BrewingProcessData()

        # Add Maischen
        mashing_step_data = MashingBrewingStepData('Maischen')
        for levels in recipe.mashing_plan.temperature_levels:
            mashing_step_data.add_rast(levels.temperature, levels.duration)
        for malt in recipe.mashing_plan.malt_additions:
            mashing_step_data.add_malt_addition(malt)
        process_data.add_brewing_step(mashing_step_data)

        # Add Laeutern
        laeuter_step_data = BrewingStepData('Läutern')
        process_data.add_brewing_step(laeuter_step_data)

        # Add Hopfenkochen
        cooking_step_data = HopCookingBrewingStepData('Hopfenkochen', recipe.hop_cooking_plan.duration)
        for hop in recipe.hop_cooking_plan.hop_additions:
            cooking_step_data.add_hop_addition(hop)
        process_data.add_brewing_step(cooking_step_data)

        # Add Whirlpool
        whirlpool_step_data = BrewingStepData('Whirlpool')
        process_data.add_brewing_step(whirlpool_step_data)

        self._brewing_process_data = process_data

        # # Setup the process.
        process = BrewingProcess()

        # Add Maischen
        mashing_step = MashingBrewingStep(mashing_step_data, self.controller)
        process.add_step(mashing_step)
        # Add lautering
        lautering_step = ConfirmationStep('LAUTERING', laeuter_step_data)
        process.add_step(lautering_step)
        # Add Hop cooking
        # todo: Use a proper step for the hop cooking
        hop_cooking_step = HopCookingBrewingStep(cooking_step_data, self.controller)
        process.add_step(hop_cooking_step)
        # Add whirlpool
        whirlpoool_step = ConfirmationStep('WHIRLPOOL', whirlpool_step_data)
        process.add_step(whirlpoool_step)

        self._process = process

    def has_open_request(self):
        return self._process.get_state().is_awaiting_answer()

    def confirm_request(self, state):
        if self._process.get_state().to_int() == state:
            self._process.confirm_request(int(time.time()))

            return True

        return False

    def get_current_state(self):
        return self._process.get_state()
