from impl.data.actor_history import ActorHistory


class Actor(object):
    def __init__(self) -> None:
        self.history = ActorHistory()

    def set_history(self, history: ActorHistory) -> None:
        self.history = history

    def store(self, time: int, state) -> None:
        self.history.add_item(time, state)

    def start(self, time: int) -> bool:
        """
        Start the actor.

        Args:
            time: Current time of calling this method.

        Returns:
            Whether starting the actor was successful.
        """
        pass

    def stop(self, time) -> bool:
        """
        Stop the actor.

        Args:
            time: Current time of calling this method.

        Returns:
            Whether stopping the actor was successful.
        """
        pass
