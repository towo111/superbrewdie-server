from impl.data.sensor_history import SensorHistory


class Sensor(object):
    def __init__(self) -> None:
        self.history = SensorHistory()

    def set_history(self, history: SensorHistory) -> None:
        self.history = history

    def store(self, time: int, value: float) -> None:
        self.history.add_item(time, value)

    def sense(self, time: int) -> bool:
        """
        Sense whatever the sensor is sensing.

        This method is called frequently to allow regular updates of the sensors.

        Args:
            time: Current time of calling this method.

        Returns:
            Whether sensing was successful.
        """
        pass

    def get_time(self):
        """
        Return the latest timestamp when sensed successfully.
        """
        pass

    def get_value(self):
        """
        Get latest sensed value.
        """
        pass
