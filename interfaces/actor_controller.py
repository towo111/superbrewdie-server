from interfaces.actor import Actor
from interfaces.sensor import Sensor


class ActorController(object):
    def __init__(self, actor: Actor, sensor: Sensor):
        self.actor = actor
        self.sensor = sensor

    def init(self):
        pass

    def exec(self, time):
        pass

    def start(self, time):
        pass

    def stop(self, time):
        pass
