import threading

from logger import logger

from impl.brewing_server import BrewingServer
from impl.data.recipe import Recipe, TemperatureLevel, MaltAddition, HopAddition

import time

from flask import Flask, Blueprint
from flask_restx import Api, Resource

from impl.process.state import State

brewingThread = None


def run_rest_api_app():
    app = Flask(__name__)
    api = Api(app)

    @api.route("/alive")
    class ApiAlive(Resource):
        def get(self):
            return True

    @api.route("/time")
    @api.doc(description='Provide current server time in seconds since epoch.')
    class ApiTime(Resource):
        def get(self):
            return int(time.time())

    @api.route("/brewing/start/<recipe>")
    @api.doc(description='Start the brewing process based on the given recipe name.')
    @api.param('recipe', 'Recipe name to start brewing.')
    class ApiBrewingStart(Resource):
        def post(self, recipe):
            global brewingThread

            if not brewingServer.is_running():
                brewingServer.init_recipe(setup_example_recipe())
                brewingThread = threading.Thread(target=runBrewingServer).start()

    @api.route("/brewing/state")
    @api.doc(description='Returns current brewing state. Defined as follows:\n'
                         '{}'.format(State.documentation()))
    class ApiBrewingState(Resource):
        def get(self):
            return brewingServer.get_current_state().to_dict()

    @api.route("/brewing/confirm/<int:state>")
    @api.param('state', 'State to confirm.\n'
                        '{}'.format(State.documentation()))
    class ApiBrewingConfirm(Resource):
        def post(self, state: int):
            if brewingServer.has_open_request():
                return brewingServer.confirm_request(int(state))
            return False

    @api.route("/brewing/temperature/<int:since>")
    @api.doc(description='Request all temperature data since given time of the current brewing step.')
    @api.param('since', 'The time stamp in seconds since epoch.')
    class ApiBrewingTemperature(Resource):
        @api.response(400, 'No ongoing brewing process.')
        @api.response(500, 'Current step does not have temperature data.')
        def get(self, since: int = 0):
            step_data = brewingServer.get_current_brewing_step_data()
            if step_data is not None:
                try:
                    temperature_history = step_data.get_sensor_history('Thermometer').history
                    valid_times = [t for t in temperature_history.keys() if t > since]
                    if len(valid_times) > 0:
                        return {valid_time: temperature_history[valid_time] for valid_time in valid_times}
                    else:
                        return {}
                except:
                    return 500
            else:
                return 400

    app.run(host='0.0.0.0')


def runBrewingServer():
    brewingServer.run(1)


def setup_example_recipe():
    recipe = Recipe('test-beer', 'Test', 'This is a test recipe!', 1641476884)

    # # Setup mashing plan
    mashing_plan = recipe.mashing_plan
    # Ingredients
    mashing_plan.malt_additions.append(MaltAddition('Pilsner Malz', 5, 'kg', 0))
    # Temperature levels
    mashing_plan.temperature_levels.append(TemperatureLevel(40, 10))
    mashing_plan.temperature_levels.append(TemperatureLevel(54, 10))
    mashing_plan.temperature_levels.append(TemperatureLevel(78, 10))

    # # Setup hop cooking plan
    hop_plan = recipe.hop_cooking_plan
    # Duration
    hop_plan.duration = 120
    # Ingredients
    hop_plan.hop_additions.append(HopAddition('SuperHop', 10, 'g', 0))
    hop_plan.hop_additions.append(HopAddition('SecretHop', 20, 'g', 30))

    return recipe


brewingServer = BrewingServer()


if __name__ == '__main__':
    try:
        logger.info(f'start WebServer')
        run_rest_api_app()
    except Exception as e:
        logger.error("Unexpected error:" + str(e))
